import { Component } from '@angular/core';
import { SocketService } from './services/socket.service';
import { Event } from './services/event-enums';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private socketService: SocketService){}
  
  userMessage: string; 
  messages: string[] = [];

  ngOnInit(){
    this.socketService.openConnection();

    //Set the client to subscribe to the 'connect' event
    this.socketService.onEventObservables(Event.CONNECT).subscribe(() => {
      this.socketService.send(Event.MESSAGE, 'A user joined')
    })

    //Set the client to subscribe to the 'message' event
    this.socketService.onEventObservables(Event.MESSAGE).subscribe((message) => {
      console.log("Received message: " + message)
      this.messages.push(message);
    })
    // this.socketService.onEvent(Event.MESSAGE, this.handleMessageEvent)
  }

  sendMessage(){
    console.log("Sending: " + this.userMessage)

    //The client emits the 'message' event and sends its message to the server
    this.socketService.send(Event.MESSAGE, this.userMessage);
    this.userMessage = "";
  }

  /* Handler function for adding messages to the message queue
  handleMessageEvent(message){
    console.log("Received message: " + message)
    this.messages.push(message);
  }
  */

}
