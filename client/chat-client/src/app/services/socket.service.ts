import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
// import { Observer } from 'rxjs/Observer';
import { Event } from './event-enums'
import * as socketIO from 'socket.io-client'

//This SERVER_URL will come from a global app-constants file (hardcoded it here for simplicity)
const SERVER_URL = "http://localhost:3000"

@Injectable({
    providedIn: 'root'
})
export class SocketService{
    private socket;

    public openConnection(): void {
        this.socket = socketIO(SERVER_URL);
    }

    public closeConnection() {
        //closes the connection and returns a Socket object
        return this.socket.close();
    }

    public send(emit_event: Event, data: any): void {
        this.socket.emit(emit_event, data);
    }

    //onEvent listener function using observables
    public onEventObservables(event : Event): Observable<any> {
        return new Observable<any>(observer => {
            this.socket.on(event, (returned_data) => returned_data ? observer.next(returned_data) : observer.next() );
        })
    }

    //onEvent listener function without using observables
    // public onEvent(event: Event, eventHandler: Function): void {
    //     this.socket.on(event, eventHandler);
    // }
}