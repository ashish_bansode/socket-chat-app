### Setup steps and commands:
1. Navigate into server folder: **cd socket-chat-app/server**

2. Install dependencies: **npm install**

3. Run server: **node index.js**
