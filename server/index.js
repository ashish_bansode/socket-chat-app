var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.send('<h1>Welcome to the chat app</h1>');
});



io.on('connection', function(socket){
    console.log("A user connected")
    socket.on('message', (msg) => {
        console.log("Server received message: " + msg)
        io.emit('message', msg)
        // socket.broadcast.emit('message', msg)
    })

    socket.on('disconnect', () => {
        console.log('A user disconnected')
        io.emit('message', 'A user disconnected')
    })
})



http.listen(3000, function(){
  console.log('chat server listening on port :3000');
});